var gulp = require('gulp');
var less = require('gulp-less');

gulp.task('cssTask', function(){
    return gulp.src('src/css/less/*.less')
    .pipe(less())
    .pipe(gulp.dest('src/css'))
});

gulp.task('watch', function() {
    gulp.watch('src/css/less/*.less', ['cssTask'])
});

gulp.task('default', [ 'css' ]);
