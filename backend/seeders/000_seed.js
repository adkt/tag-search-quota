const Promise = require('bluebird');

module.exports = {
    up: (query, DataTypes) => {
        return Promise.all([
            querye.bulkInsert('tableName',
            [{
              "password": "LnRYCjV",
              "email": "mhumbles0@paypal.com",
              "firstName": "Marney",
              "lastName": "Humbles",
              "gender": "Female",
              "job": "Nuclear Power Engineer",
              "street": "39566 Prentice Alley",
              "city": "Kurkino",
              "bio": "Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.",
              "color": "#d7dd7c",
              "website": "https://typepad.com/suscipit/ligula.js"
            }, {
              "password": "O9WLUxHF",
              "email": "adibdin1@reuters.com",
              "firstName": "Arleyne",
              "lastName": "Dibdin",
              "gender": "Female",
              "job": "Junior Executive",
              "street": "961 Ryan Way",
              "city": "Kampala",
              "bio": "Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl.",
              "color": "#415d95",
              "website": "http://newyorker.com/nec/condimentum/neque/sapien/placerat/ante.aspx"
            }, {
              "password": "aEMC6i",
              "email": "dmcdonough2@cbslocal.com",
              "firstName": "Daryl",
              "lastName": "McDonough",
              "gender": "Female",
              "job": "Teacher",
              "street": "184 Grover Alley",
              "city": "Yefremov",
              "bio": "Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.",
              "color": "#cc1781",
              "website": "https://mozilla.org/nibh/quisque.jsp"
            }, {
              "password": "QYtTGrPYN",
              "email": "wskea3@skype.com",
              "firstName": "Worthington",
              "lastName": "Skea",
              "gender": "Male",
              "job": "Structural Engineer",
              "street": "52 Manitowish Street",
              "city": "Youwei",
              "bio": "In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.",
              "color": "#084f15",
              "website": "http://usatoday.com/sed/tincidunt/eu/felis/fusce/posuere/felis.jsp"
            }, {
              "password": "gJgkXWQlJ5N",
              "email": "mpodbury4@netvibes.com",
              "firstName": "Melony",
              "lastName": "Podbury",
              "gender": "Female",
              "job": "Account Executive",
              "street": "3 Rutledge Pass",
              "city": "Quixeramobim",
              "bio": "Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.",
              "color": "#af11b0",
              "website": "http://japanpost.jp/rhoncus.jpg"
            }, {
              "password": "RR6fs6",
              "email": "agrendon5@biblegateway.com",
              "firstName": "Antonin",
              "lastName": "Grendon",
              "gender": "Male",
              "job": "Administrative Assistant I",
              "street": "396 Forest Run Pass",
              "city": "Reda",
              "bio": "Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
              "color": "#6e2688",
              "website": "https://disqus.com/tempus/sit/amet/sem/fusce/consequat/nulla.html"
            }, {
              "password": "bxzQB9f8QBR",
              "email": "anaul6@disqus.com",
              "firstName": "Akim",
              "lastName": "Naul",
              "gender": "Male",
              "job": "Research Assistant IV",
              "street": "8 Forest Run Alley",
              "city": "Tuen Mun",
              "bio": "Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.",
              "color": "#82db27",
              "website": "https://biglobe.ne.jp/penatibus/et/magnis.png"
            }, {
              "password": "AlDH0K",
              "email": "oarrandale7@hud.gov",
              "firstName": "Osbert",
              "lastName": "Arrandale",
              "gender": "Male",
              "job": "Legal Assistant",
              "street": "55 Washington Court",
              "city": "Saga",
              "bio": "Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.",
              "color": "#27673e",
              "website": "http://indiegogo.com/neque/sapien/placerat/ante/nulla/justo/aliquam.aspx"
            }, {
              "password": "7Le6DqBes",
              "email": "ktesoe8@weibo.com",
              "firstName": "Krisha",
              "lastName": "Tesoe",
              "gender": "Male",
              "job": "VP Marketing",
              "street": "81156 Golden Leaf Hill",
              "city": "Dongming Chengguanzhen",
              "bio": "Sed ante. Vivamus tortor.",
              "color": "#058742",
              "website": "https://reddit.com/rhoncus/mauris/enim/leo.xml"
            }, {
              "password": "iHfoKv",
              "email": "iloosmore9@live.com",
              "firstName": "Ileane",
              "lastName": "Loosmore",
              "gender": "Female",
              "job": "Software Test Engineer IV",
              "street": "61 Mesta Way",
              "city": "Ciherang",
              "bio": "In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem.",
              "color": "#f69497",
              "website": "https://census.gov/nulla/quisque/arcu/libero/rutrum/ac/lobortis.js"
            }, {
              "password": "ABYNxA6Dq",
              "email": "asuttella@harvard.edu",
              "firstName": "Analiese",
              "lastName": "Suttell",
              "gender": "Female",
              "job": "Food Chemist",
              "street": "55 Eastlawn Place",
              "city": "San Jose",
              "bio": "In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.",
              "color": "#28901c",
              "website": "http://marriott.com/nullam/orci/pede/venenatis/non/sodales.jsp"
            }, {
              "password": "J2KsWAHrx0QA",
              "email": "dsyresb@gov.uk",
              "firstName": "Denny",
              "lastName": "Syres",
              "gender": "Male",
              "job": "Librarian",
              "street": "240 Delladonna Alley",
              "city": "Tomioka",
              "bio": "Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue.",
              "color": "#f480c1",
              "website": "http://privacy.gov.au/id.json"
            }, {
              "password": "xPcyh4f",
              "email": "dbroadfieldc@dot.gov",
              "firstName": "Dawn",
              "lastName": "Broadfield",
              "gender": "Female",
              "job": "Product Engineer",
              "street": "4 Tennyson Center",
              "city": "Eirol",
              "bio": "Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.",
              "color": "#c47687",
              "website": "http://furl.net/vestibulum.jsp"
            }, {
              "password": "s3WDE5oHtYP",
              "email": "nmoffatd@wordpress.com",
              "firstName": "Nari",
              "lastName": "Moffat",
              "gender": "Female",
              "job": "Food Chemist",
              "street": "87 Shoshone Street",
              "city": "Vyritsa",
              "bio": "Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.",
              "color": "#993f24",
              "website": "https://feedburner.com/morbi/odio/odio.html"
            }, {
              "password": "N1ZsO13n",
              "email": "ckobpale@ibm.com",
              "firstName": "Chrissy",
              "lastName": "Kobpal",
              "gender": "Male",
              "job": "Programmer III",
              "street": "7753 Brentwood Junction",
              "city": "Greytown",
              "bio": "Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.",
              "color": "#703334",
              "website": "https://telegraph.co.uk/blandit/ultrices/enim/lorem.jpg"
            }, {
              "password": "r7hQdS",
              "email": "hbirtleyf@ask.com",
              "firstName": "Hagen",
              "lastName": "Birtley",
              "gender": "Male",
              "job": "Structural Analysis Engineer",
              "street": "224 Orin Plaza",
              "city": "Jindui",
              "bio": "In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.",
              "color": "#46868b",
              "website": "https://ibm.com/turpis/integer.json"
            }, {
              "password": "cDFVCvU5",
              "email": "astockaug@google.ru",
              "firstName": "Antonio",
              "lastName": "Stockau",
              "gender": "Male",
              "job": "Actuary",
              "street": "1182 Corben Street",
              "city": "Gharavŭtí",
              "bio": "Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit.",
              "color": "#b4cc5a",
              "website": "https://behance.net/auctor.jsp"
            }, {
              "password": "XmeKXhGmS3un",
              "email": "lwillersh@japanpost.jp",
              "firstName": "Luca",
              "lastName": "Willers",
              "gender": "Male",
              "job": "Registered Nurse",
              "street": "2 Morningstar Road",
              "city": "Kiltamagh",
              "bio": "Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula.",
              "color": "#044980",
              "website": "https://cocolog-nifty.com/nec/condimentum/neque/sapien/placerat/ante/nulla.png"
            }, {
              "password": "odTIFEPIXnsF",
              "email": "cdilloni@yellowpages.com",
              "firstName": "Court",
              "lastName": "Dillon",
              "gender": "Male",
              "job": "Design Engineer",
              "street": "13441 Tomscot Court",
              "city": "Cortiços",
              "bio": "Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum.",
              "color": "#116ee2",
              "website": "http://dyndns.org/nullam/porttitor/lacus/at.js"
            }, {
              "password": "MBVNNHH6X",
              "email": "nwingrovej@jimdo.com",
              "firstName": "Nisse",
              "lastName": "Wingrove",
              "gender": "Female",
              "job": "Senior Cost Accountant",
              "street": "4 Waubesa Place",
              "city": "Srubec",
              "bio": "Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus.",
              "color": "#eecf63",
              "website": "https://exblog.jp/risus/auctor/sed/tristique/in.jpg"
            }]
        );
    },

    down: (query, DataTypes) => {
        // return query.dropAllTables();
        return query.dropTable('DbProperties');
    }
};
