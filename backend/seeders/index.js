const Sequelize = require('sequelize');
const Promise = require("bluebird");
const Umzug = require('umzug');

var config = require('../beConfig.json');

const sequelize = new Sequelize(config.dbName, config.dbUser, config.dbPass, {
    host: 'localhost:'+config.dbPort,
    dialect: config.dbType,
    // connection pool
    pool: {
        max: 10,
        min: 0,
        idle: 10000
    },
    storage: './backend/'+config.sqliteDbFile
});

var umzug = new Umzug({
    storage: 'json',
    storageOptions: {path: process.cwd() + '/backend/seeder_index.json'},
    sequelize: sequelize,
    migrations: {
        path: './backend/seeders',
        pattern: /^(?!.*(index\.js|root\.js)).*$/,
        params: [sequelize.getQueryInterface(), Sequelize]
    }
});

var run = function ()
{
    if(config.environment.toLowerCase() == "dev" || config.environment.toLowerCase() == "development")
    {
        umzug.pending().then(function (seeds) {
            console.log(seeds);
        });
        umzug.up().then(function (seeds) {
          console.log(seeds);
        });
    }
}

var seeder = {};
module.exports = seeder;
module.exports.run = run;
