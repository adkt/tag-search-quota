var helmet = require('helmet');
var session = require('express-session');
var bodyParser = require('body-parser');

exports.init = function(app, port)
{
	// allow cors access from any external domain
	app.all('/', function(req, res, next) {
    	res.header("Access-Control-Allow-Origin", "*");
    	res.header("Access-Control-Allow-Headers", "X-Requested-With");
    	next();
	});

	// starts the server listening
	app.listen(port, function(){
		console.log('listening on port '+port+'...');
	});
}

exports.addHelmet = function(app)
{
	app.use(helmet());
	app.use(helmet.noCache());
}

exports.parsers = function(app)
{
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({extended: true}));
}
