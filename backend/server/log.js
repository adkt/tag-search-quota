var morgan = require('morgan');

exports.init = function(app)
{
	// initialise http logging
	// errors to std error
	app.use(morgan('dev', {
		skip: function (req, res) {
			return res.statusCode < 400
		},
		stream: process.stderr
	}));

	// sucesses to std output
	app.use(morgan('dev', {
    	skip: function (req, res) {
        	return res.statusCode >= 400
		},
		stream: process.stdout
	}));
}
