module.exports = (sequelize, DataTypes) => {
    var DbProperties = sequelize.define('DbProperties', {
        version: {
            type: DataTypes.INTEGER,
        }
    }, {
        getterMethods: {
            dbVersion() {
                // cannot return just one parameter e.g. just 'this.version' will cause an error
                return "Database Version: " + this.version;
            }
        }
    });

    /*
    DbProperties.associate = function (models){
        DbProperties.belongsTo(models.user, {as: 'user', foreignKey: 'userId');
    }
    */

    return DbProperties;
}
