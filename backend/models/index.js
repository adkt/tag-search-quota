// require node packages
const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
var Promise = require("bluebird");
var Umzug = require('umzug');

var config = require('../beConfig.json');

const sequelize = new Sequelize(config.dbName, config.dbUser, config.dbPass, {
    host: 'localhost:'+config.dbPort,
    dialect: config.dbType,
    // connection pool
    pool: {
        max: 10,
        min: 0,
        idle: 10000
    },
    storage: './backend/'+config.sqliteDbFile
});

var umzug = new Umzug({
    storage: 'json',
    storageOptions: {path: process.cwd() + '/backend/migration_index.json'},
    sequelize: sequelize,
    migrations: {
        path: './backend/migrations',
        pattern: /\.js$/,
        params: [sequelize.getQueryInterface(), Sequelize]
    }
});


/******************** start of database object *******************************/
var db = {};

// loader: requires all database models in the folder and store them in the db object
fs.readdirSync(__dirname).filter(function(file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
}).forEach(function(file) {
    var model = sequelize.import(path.join(__dirname, file));
    console.log("imported db Model: "+model.name);
    db[model.name] = model;
});

/*
Object.keys(db).forEach(function(modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});
*/

// Enable cancellation
Promise.config({cancellation: true});

// For SQLite only - check the db exists, if not then sync the database
var initDb = function ()
{
    if (config.dbType === "sqlite")
    {
        var promiseChain = firstOpenDb(config.sqliteDbFile)
            .then((result) => {
                if (result){
                    initMigration();
                    //console.log("DB exists");
                    //getDbVersion();
                    return promiseChain.cancel();
                }

                //Run database migration 000_initial
                //initMigration();

                /*
                db.sequelize.sync()
                    .then(() => {
                        return db.DbProperties.create({
                            version: 1,
                        });
                    }).then(()=>{
                        getDbVersion();
                    })
                */
            });

    }
}

var firstOpenDb = function (dbName) {
    var promise = new Promise((resolve, reject)=>{
        fs.access('./backend/backend.sqlite3', fs.constants.W_OK, (err) => {
            if (err)
            {
                if (err.code === "ENOENT")
                {
                    console.log("DB: sqlitedb file does not exist.");
                    resolve(false);
                }
            } else {
                resolve(true);
            }
        });

    });
    return promise;
}

var getDbVersion = function () {
    db.DbProperties.findAll()
    .then((all) => {
        // get first item returned
        var item = all[0];
        // Check if record exists in db
        if (item) {
            console.log(item.dbVersion);
            /*
            item.version = 1;
            item.save().then((item, err) => {
                if (err)
                {
                    console.log(err);
                } else {
                    console.log(item.dbVersion);
                }
            });
            */
        }
    });
}

var initMigration = function () {
    /*
    umzug.pending().then(function (migrations) {
        console.log(migrations);
    });
    */
    umzug.execute({
        migrations: ['000_initial.js'],
        method: 'up'
    }).then(function (migrations) {
        console.log(migrations);
    });
}



// export database class
db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
module.exports.initDb = initDb;
