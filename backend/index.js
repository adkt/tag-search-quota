// Get node modules
var express = require('express');

// Get engine
var server = require('./server/server.js');
var logger = require('./log/logger.js');
var httpLog = require('./server/log.js');
var routes = require('./server/routes.js');
var db = require('./models/index.js');
var seeder = require('./seeders/index.js');
var config = require('./beConfig.json');

/*
// loader: requires all database models in the folder
var model = {};
var modelPath = require("path").join(__dirname, "models");
require("fs").readdirSync(modelPath).forEach(function(file) {
  model[file] = require("./models/" + file);
});
*/

// define the app/server
var app = express();

// initialise http logging
httpLog.init(app);

// initialise helmet
server.addHelmet(app);

// initialise parsers
server.parsers(app);

// initialise server
server.init(app, config.expressPort);

// initialise routes
routes.init(app);

// initialise SQLite database
//model['initDb.js'].load(app, config.sqlDb, config.expressPort);

/*
db.sequelize.sync({
  // force: true
});
db.DbProperties.findAll().then(function (stores) {
    console.log(stores);
});
*/
//db.initDb();
seeder.run();

// export express app so we can test it
module.exports = app;
