const Promise = require('bluebird');

module.exports = {
    up: (query, DataTypes) => {
        return Promise.all([
            query.createTable('DbProperties', {
                id: {
                    type: DataTypes.INTEGER,
                    allowNull: false,
                    primaryKey: true,
                    autoIncrement: true
                },
                version: {
                    type: DataTypes.INTEGER,
                    allowNull: false
                },
                createdAt: {
                    type: DataTypes.DATE,
                    allowNull: false
                },
                updatedAt: {
                    type: DataTypes.DATE,
                    allowNull: false
                },
            }),
            query.createTable('Users', {
            	password:DataTypes.STRING,
            	email: DataTypes.STRING,
            	firstName: DataTypes.STRING,
            	lastName: DataTypes.STRING,
            	gender: DataTypes.STRING,
            	job: DataTypes.STRING,
            	street: DataTypes.STRING,
            	city: DataTypes.STRING,
            	bio: DataTypes.TEXT,
            	color: DataTypes.STRING,
            	website: DataTypes.TEXT
            })
    },

    down: (query, DataTypes) => {
        // return query.dropAllTables();
        return query.dropTable('DbProperties');
    }
};
