var winston = require("winston");

var level = process.env.LOG_LEVEL || 'debug';

// Log to terminal console
var logger = new winston.Logger({
	transports: [ new winston.transports.Console({
		level: level,
		timestamp: function () {
			return (new Date()).toString();
		}
	})]
});

/*
// Log to file
var logger = new (winston.Logger) ({
	transports: [ new(winston.transports.File) ({
    	filename: 'log/log.txt',
    	handleExceptions: true,
    	prettyPrint: true,
		level: level,
		timestamp: function (){
			return (new Date()).toString();
		}
    })]
});
*/

module.exports = logger;
