import React from 'react';
import { connect } from "react-redux"

import Header from './Header';

import { fetchUser } from "../dispatch/user"


@connect((store) => {
    return {
        people: store.user.people
    };
})

export default class PageHome extends React.Component {
    constructor() {
        super();
        this.state = {
            pageTitle: "Welcome",
        };
    }

    // once the component is loaded into virtual DOM then run this function
    componentWillMount() {
        this.props.dispatch(fetchUser())
    }

    // *** important: when creating SMART child components remember to pass the dispatcher as a prop if you want to dispatch actions from these children !!!!
    render(){
        const user = this.props;
        console.log(user.people);
        return (
            <div>
                {(typeof user.people[0] !== 'undefined') ? <h1>{user.people[0].name}</h1> : <h1>empty</h1>}
                <Header
                    pageTitle={this.state.pageTitle}
                    
                    dispatcher={this.props.dispatch}
                />
            </div>
        );
    }
}
