import React from 'react';
import {Bootstrap, Grid, Row, Col} from 'react-bootstrap';

// CSS
import 'bootstrap/dist/css/bootstrap.css';

import '../../css/less/main.less';

// React-Bootstrap components
import { Navbar } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { FormGroup } from 'react-bootstrap';
import { FormControl } from 'react-bootstrap';

// dispatch Actions
import { logName } from "../dispatch/user"

export default class Header extends React.Component {
    constructor() {
        super();
        this.state = {
            title: 'My Header',
            name: '',
        };
    }

    /* // example of onChange binding
    handleChange(e) {
        const title = e.target.value;
        this.props.changeTitle(title);
    }
    */

    sendData(name, dispatcher) {
        dispatcher(logName(name))
    }

    render() {
        return (
            <div class="root">

                {/*
                    <input value={this.props.title} onChange={this.handleChange.bind(this)} />
                */}
                {/*Header*/}
                <Navbar class='headerNavbar container' inverse collapseOnSelect>
                    <Navbar.Header>
                        <Navbar.Brand>
                        <a href="#brand">React-Bootstrap</a>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                </Navbar>

                {/*Body*/}
                <div class="container fill background mainContainer">
                    <h1 class="my-4">{this.props.pageTitle}
                        <small> Example User Portal</small>
                    </h1>

                    <FormGroup>
                        <FormControl
                            type="text" value={this.state.name} placeholder="Enter text"
                            onChange={(event) => this.setState({name: event.target.value})}
                        />
                    </FormGroup>

                    <Button type="submit" onClick={this.sendData.bind(this, this.state.name, this.props.dispatcher)}>Send Data</Button>

                </div>

            </div>
        );
    }

}
