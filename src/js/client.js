import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";

import PageHome from './components/PageHome';

import store from "./store";

ReactDOM.render(
    <Provider store={store}>
        <PageHome />
    </Provider>, document.getElementById('app')
);
