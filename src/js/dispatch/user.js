export function fetchUser() {
    return function(dispatch) {


        var response = {};
        response.data = [{
            id: 1,
            name: "Adam",
            hobbies:"Web Development, Pokemon GO, Researching new technologies",
            favFood: "Lychee",
            other: "Chewbacca is cool"
        },{
            id: 2,
            name: "Nicole",
            hobbies:"Studying Health, Pokemon GO, Pilates",
            favFood: "Buffalo Wings",
            other: "I love Eevee from Pokemon"
        }];

        dispatch({type: "FETCH_USER"});

        setTimeout(function() {
            dispatch({type: "FETCH_USER_FULFILLED", payload: response.data})
        }.bind(this), 1000);


    }
}

export function logName(name){
    if(typeof name !== 'undefined')
    {
        return function (dispatch){
            setTimeout(function() {
                dispatch({type: "SEND_USER_FULFILLED", payload: name})
            }.bind(this), 1000);
        }
    }
}
