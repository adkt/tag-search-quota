export default function reducer(state={
    people: {},
    fetching: false,
    fetched: false,
    error: null,
}, action) {

    switch (action.type) {
        case 'FETCH_USER': {
            return {...state, fetching: true}
        }

        case 'FETCH_USER_REJECTED': {
            return {...state, fetching: false, error: action.payload}
        }

        case 'FETCH_USER_FULFILLED': {
            return {
                ...state,
                fetching: false,
                fetched: true,
                people: action.payload,
            }
        }

        case 'SEND_USER_FULFILLED':{
            return{
                ...state,
                fetching: false, fetched: true,
                name: action.payload,
            }
        }
    }

    return state;
}
